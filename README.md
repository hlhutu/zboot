# 目录结构
~~~
zboot
 ├── common                         　// 通用模块
 │     ├── devtools                   // 开发工具
 │     │      └── gen-code            // 代码生成器
 │     ├── common-datascope           // [x]数据权限
 │     ├── common-datasource          // 数据源，支持多数据源
 │     ├── common-datasource-sharding // 数据源，支持分片
 │     ├── common-es                  // es全文检索
 │     ├── common-mq                  // rocketmq消息队列
 │     ├── common-redis               // redis数据库
 │     ├── common-server-base         // 通用组件，所有服务引入
 │     ├── common-log                 // [x]日志记录
 │     ├── common-xxl-job             // [x]xxl-job调度
 │     ├── common-security            // [x]安全模块
 │     ├── common-swagger             // [x]系统接口
 │
 ├── gateway                          // 网关[8080]
 ├── z-openform                       // 动态表单
 │     ├── z-openform-facade
 │     └── z-openform-service
 ├── z-report                         // 报表设计器
 │     ├── z-system-facade
 │     └── z-system-service
 ├── z-system                         // 系统模块
 │     ├── z-system-facade
 │     └── z-system-service
 │
 ├── z-ui                             // 前端 [80]
 └── pom.xml
~~~

## 结构说明
~~~
* z-comm包下的所有模块为工具模块，以jar包的方式引入
* z-services包下的所有模块为微服务，需要以服务的方式启动
* 所有微服务分为2个子模块，facade和service。
*   ├── facade  提供远程调用接口，并包含所需的实体类，静态等
*   └── service 则是服务本身，需要启动提供服务。一般service都需要引入facade使用
~~~
