package com.zboot.comm.es.util;

/**
 * SLOGAN:让现在编程未来
 *
 * @author <a href="mailto:linfeng@dtstack.com">林丰</a> 2019/8/5.
 */
public enum PredicateEnum {
    MUST,
    MUST_NOT,
    SHOULD
}
