package com.zboot.comm.redis.ann;

import java.lang.annotation.*;

/**
 * redis缓存刷新，有此注解的方法将会刷新缓存
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface EnableRedisCacheFresh {

    String value();
}
