package com.zboot.comm.redis.ann;

import java.lang.annotation.*;

/**
 * redis缓存，有此注解的方法将会进行缓存
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface EnableRedisCache {

    String value();
}
