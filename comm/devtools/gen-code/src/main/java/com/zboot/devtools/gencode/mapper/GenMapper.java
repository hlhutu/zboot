package com.zboot.devtools.gencode.mapper;

import java.util.List;

public interface GenMapper {

    List<String> listTableNames();
}
