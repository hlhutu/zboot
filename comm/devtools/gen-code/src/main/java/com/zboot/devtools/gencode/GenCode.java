package com.zboot.devtools.gencode;

import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.zboot.devtools.gencode.mapper.GenMapper;
import com.zboot.devtools.gencode.util.MybatisUtils;
import org.apache.ibatis.session.SqlSession;

import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Scanner;

public class GenCode {

    public static void main(String[] args) throws InterruptedException {
        /**
         * 使用之前请设置
         */
        String serverName = "z-system";
        String prefix = "z-";

        final String simpleName = serverName.replaceAll(prefix, "");
        final String outputPath = "D:/ideaprojects/zboot/"+serverName+"/"+serverName+"-service/src/main/java/";
        final String outputPathResources = "D:/ideaprojects/zboot/"+serverName+"/"+serverName+"-service/src/main/resources/";
        final String outputPathFacade = "D:/ideaprojects/zboot/"+serverName+"/"+serverName+"-facade/src/main/java/";
        final String packagePath = "com/zboot/" + simpleName;
        final String packageName = packagePath.replaceAll("\\/", ".");
        String author = "zhu";

        //查询所有表
        //SqlSession sqlSession = MybatisUtils.openSession();
        //List<String> tableNames = sqlSession.getMapper(GenMapper.class).listTableNames();
        List<String> tableNames = Arrays.asList(new String[]{"sys_user"});

        System.out.println(String.format("服务名：%s，简称%s\r\n" +
                "包路径：%s\n" +
                "包名：%s\n" +
                "service输出路径：%s\n" +
                "service资源文件输出路径：%s\n" +
                "facade输出路径：%s\n" +
                "生成表：%s\r" +
                "输入服务名继续。"
                    , serverName, simpleName, outputPath, outputPathResources, outputPathFacade, packagePath, packageName
                    , String.join(",", tableNames)
                ));
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        if(!serverName.equalsIgnoreCase(s) && !simpleName.equalsIgnoreCase(s)) {
            return;
        }

        //全局配置
        GlobalConfig config = new GlobalConfig.Builder()
                //作者
                .author(author)
                // 生成路径，最好使用绝对路径，window路径是不一样的
                .outputDir(outputPath)
                // 文件覆盖
                .fileOverride()
                //设置时间对应类型
                .dateType(DateType.ONLY_DATE)
                .enableSwagger()//允许swagger
                .build();

        //包名策略配置
        PackageConfig packageConfig = new PackageConfig.Builder()
                .parent(packageName)
                .mapper("mapper")
                .service("service")
                .serviceImpl("service.impl")
                .controller("controller")
                .entity("domain")
                .xml("mapper.xml")
                .build();

        //策略配置
        StrategyConfig strategyConfig = new StrategyConfig.Builder()
                /** 全局配置 */
                .addInclude(tableNames.toArray(new String[]{}))//生成的表明
                .addTablePrefix("t_")//表前缀
                .enableCapitalMode()//策略开启驼峰命名
                /** 实体类配置 */
                .entityBuilder()
                    .superClass("com.zboot.comm.web.domain.BaseEntity")
                    .enableLombok()//允许lombok
                    .columnNaming(NamingStrategy.underline_to_camel)//表明与字段名全部驼峰
                    .naming(NamingStrategy.underline_to_camel)
                /** mapper文件配置 */
                .mapperBuilder()//mapper类添加@Mapper
                    //生成基本的SQL片段
                    //.enableBaseColumnList()
                    //生成基本的resultMap
                    //.enableBaseResultMap()
                /** service配置 */
                .serviceBuilder()
                    .formatServiceFileName("%sService")//格式化文件名
                /** controller配置 */
                .controllerBuilder()
                    .enableRestStyle()
                .build();

        ResourceBundle rb = ResourceBundle.getBundle("jdbc"); //不要加后缀
        // 数据源配置
        DataSourceConfig.Builder dataSourceConfigBuilder = new DataSourceConfig
                .Builder(rb.getString("jdbc.url"), rb.getString("jdbc.username"), rb.getString("jdbc.password"));

        // 创建代码生成器对象，加载配置
        AutoGenerator autoGenerator = new AutoGenerator(dataSourceConfigBuilder.build());
        autoGenerator.global(config);
        autoGenerator.packageInfo(packageConfig);
        autoGenerator.strategy(strategyConfig);

        //执行操作
        autoGenerator.execute();
        System.out.println("=======  Done 相关代码生成完毕  ========");
    }
}
