package com.zboot.comm.mq;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.common.RemotingHelper;

import java.util.Scanner;

public class App {

    public static void main(String[] args) throws Exception {
        // 实例化消息生产者Producer
        DefaultMQProducer producer = new DefaultMQProducer("TEST_MQ");
        // 设置NameServer的地址
        producer.setNamesrvAddr("192.168.2.170:9876");
        // 启动Producer实例
        producer.start();
        for (int i = 0; i < 5; i++) {
            System.out.println("请输入消息");
            Scanner scanner = new Scanner(System.in);
            String s = scanner.nextLine();
            // 创建消息，并指定Topic，Tag和消息体
            Message msg = new Message("TopicTest" /* Topic */,
                    "TagA" /* Tag */,
                    s.getBytes(RemotingHelper.DEFAULT_CHARSET) /* Message body */
            );
            // 发送消息到一个Broker
            SendResult sendResult = producer.send(msg);
            // 通过sendResult返回消息是否成功送达
            System.out.printf("%s%n", sendResult);
        }
        producer.shutdown();
    }
}
