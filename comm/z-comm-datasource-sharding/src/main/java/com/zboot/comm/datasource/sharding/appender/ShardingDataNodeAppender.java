package com.zboot.comm.datasource.sharding.appender;

import org.apache.shardingsphere.sharding.rule.TableRule;

/**
 * 节点扩展接口，实现这个接口的类将会被定时任务调用以刷新节点
 * 先判断是否需要刷新
 * 如有需要，才会进行刷新
 */
public interface ShardingDataNodeAppender {

    /**
     * 需要刷新的逻辑表
     * @return
     */
    String getLogicTableName();

    /**
     * 判断是否需要追加
     * @param tableRule
     * @return
     */
    boolean needAppend(TableRule tableRule);

    /**
     * 追加节点方法
     * @param tableRule
     */
    void append(TableRule tableRule);

    /**
     * 单次追加的节点数，默认1个节点
     * @return
     */
    default int appendSize() {
        return 1;
    }

    /**
     * 最大节点数，包含模板节点，所有的表数量不能超过这个值
     * 默认20个节点
     * @return
     */
    default int maxSize() {
        return 20;
    }
}
