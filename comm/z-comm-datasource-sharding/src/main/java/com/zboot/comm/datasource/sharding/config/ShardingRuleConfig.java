package com.zboot.comm.datasource.sharding.config;

import org.apache.shardingsphere.driver.jdbc.core.datasource.ShardingSphereDataSource;
import org.apache.shardingsphere.infra.metadata.ShardingSphereMetaData;
import org.apache.shardingsphere.mode.metadata.MetaDataContexts;
import org.apache.shardingsphere.sharding.rule.ShardingRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ShardingRuleConfig {

    //初始化配置对象
    @Bean
    public ShardingRule shardingRule(ShardingSphereDataSource shardingSphereDataSource) {
        MetaDataContexts metaDataContexts = shardingSphereDataSource.getContextManager().getMetaDataContexts();
        ShardingSphereMetaData logicDb = metaDataContexts.getMetaData("logic_db");
        return logicDb.getRuleMetaData().findSingleRule(ShardingRule.class).get();//sharding配置对象
    }
}
