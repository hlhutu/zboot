package com.zboot.comm.web.domain;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class TreeEntity extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /** 父菜单ID */
    private String id;

    /** 父菜单名称 */
    private String name;

    /** 显示顺序 */
    private Integer sortNo;

    /** 祖级列表 */
    private String ancestors;

    /** 子部门 */
    private List<?> children = new ArrayList<>();
}
