package com.zboot.comm.web.page;

import com.github.pagehelper.PageHelper;
import com.zboot.comm.utils.StringUtils;
import com.zboot.comm.utils.ServletUtils;
import com.zboot.comm.utils.SqlUtil;

/**
 * 表格数据处理
 *
 * @author zhu
 */
public class PageSupport {
    public static final Integer PAGE_NUM_DEFAULT = 1;//默认页码
    public static final Integer PAGE_SIZE_DEFAULT = 20;//默认每页记录数

    /**
     * 当前记录起始索引
     */
    public static final String PAGE_NUM = "pageNum";

    /**
     * 每页显示记录数
     */
    public static final String PAGE_SIZE = "pageSize";

    /**
     * 排序列
     */
    public static final String ORDER_BY_COLUMN = "orderByColumn";

    /**
     * 排序的方向 "desc" 或者 "asc".
     */
    public static final String IS_ASC = "isAsc";

    /**
     * 封装分页对象
     */
    public static PageDomain getPageDomain() {
        PageDomain pageDomain = new PageDomain();
        pageDomain.setPageNum(ServletUtils.getParameterToInt(PAGE_NUM));
        if(StringUtils.isNull(pageDomain.getPageNum())){
            pageDomain.setPageNum(PAGE_NUM_DEFAULT);
        }
        if(pageDomain.getPageNum()<=1){
            pageDomain.setPageNum(1);//页码最小为1
        }
        pageDomain.setPageSize(ServletUtils.getParameterToInt(PAGE_SIZE));
        if(StringUtils.isNull(pageDomain.getPageSize())){
            pageDomain.setPageSize(PAGE_SIZE_DEFAULT);
        }
        if(pageDomain.getPageNum()<=0){
            pageDomain.setPageNum(0);//每页条数最小为0
        }
        pageDomain.setOrderByColumn(ServletUtils.getParameter(ORDER_BY_COLUMN));
        pageDomain.setIsAsc(ServletUtils.getParameter(IS_ASC));
        return pageDomain;
    }

    public static PageDomain buildPageRequest() {
        return getPageDomain();
    }

    public static void startPage() {
        PageDomain pageDomain = PageSupport.buildPageRequest();
        Integer pageNum = pageDomain.getPageNum();
        Integer pageSize = pageDomain.getPageSize();
        if (StringUtils.isNotNull(pageNum) && StringUtils.isNotNull(pageSize)) {
            String orderBy = SqlUtil.escapeOrderBySql(pageDomain.getOrderBy());
            PageHelper.startPage(pageNum, pageSize, orderBy);
        }
    }

    public static void startPage(int pageNum, int pageSize) {
        if (StringUtils.isNotNull(pageNum) && StringUtils.isNotNull(pageSize)) {
            PageHelper.startPage(pageNum, pageSize);
        }
    }

    public static int getPageNum() {
        PageDomain pageDomain = PageSupport.buildPageRequest();
        Integer pageNum = pageDomain.getPageNum();
        return pageNum;
    }

    public static int getPageSize() {
        PageDomain pageDomain = PageSupport.buildPageRequest();
        Integer pageSize = pageDomain.getPageSize();
        return pageSize;
    }
}
