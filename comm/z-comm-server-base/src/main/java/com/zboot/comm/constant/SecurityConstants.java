package com.zboot.comm.constant;

/**
 * 认证用的常量
 *
 * @author zhu
 */
public class SecurityConstants {
    /**
     * 令牌自定义标识
     */
    public static final String HEADER = "Authorization";

    /**
     * 令牌前缀
     */
    public static final String TOKEN_PREFIX = "Bearer ";

    /**
     * 权限缓存前缀
     */
    public final static String LOGIN_TOKEN_KEY = "login_tokens:";

    /**
     * 用户ID字段
     */
    public static final String DETAILS_USER_ID = "user_id";

    /**
     * 用户名字段
     */
    public static final String DETAILS_USERNAME = "username";

    /**
     * 真实操作人账号
     */
    public static final String DETAILS_REALNAME = "realname";

    /**
     * 用户名字段
     */
    public static final String DETAILS_NICKNAME = "nickname";

    /**
     * 虚拟用户id
     */
    public static final String DETAILS_VISUAL_USER_ID = "visual_user_id";

    /**
     * 授权信息字段
     */
    public static final String AUTHORIZATION_HEADER = "authorization";


}
