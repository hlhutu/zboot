package com.zboot.comm.web.page;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.zboot.comm.web.domain.AjaxResult;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 表格分页数据对象
 *
 * @author scihi
 */
@Data
@ApiModel
public class AjaxPageResult<T> extends AjaxResult {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("总条数")
    private long total;

    @ApiModelProperty("当前页码")
    private long pageNum;

    @ApiModelProperty("每页记录数")
    private long pageSize;

    //@ApiModelProperty("分页数据")
    //private List<T> rows;

    public AjaxPageResult() {
        super();
        this.setData(null);//rows等价于父类的data
        this.setTotal(0L);
        this.setPageNum(0);
        this.setPageSize(0);
    }

    public static <T> AjaxPageResult<T> success(Page page){
        AjaxPageResult<T> p = new AjaxPageResult<T>();
        p.setData(page.getResult());
        p.setTotal(page.getTotal());
        p.setPageNum(page.getPageNum());
        p.setPageSize(page.getPageSize());
        return p;
    }

    public static <T> AjaxPageResult<T> success(String msg, Page page){
        AjaxPageResult<T> p = new AjaxPageResult<T>();
        p.setMsg(msg);
        p.setData(page.getResult());
        p.setTotal(page.getTotal());
        p.setPageNum(page.getPageNum());
        p.setPageSize(page.getPageSize());
        return p;
    }

    public static <T> AjaxPageResult<T> success(List<T> list){
        AjaxPageResult<T> p = new AjaxPageResult<T>();
        PageInfo page = new PageInfo(list);
        p.setData(list);
        p.setTotal(page.getTotal());
        p.setPageNum(page.getPageNum());
        p.setPageSize(page.getPageSize());
        return p;
    }

    public static <T> AjaxPageResult<T> success(String msg, List list){
        AjaxPageResult<T> p = new AjaxPageResult<T>();
        p.setMsg(msg);
        PageInfo page = new PageInfo(list);
        p.setData(list);
        p.setTotal(page.getTotal());
        p.setPageNum(page.getPageNum());
        p.setPageSize(page.getPageSize());
        return p;
    }

    public AjaxPageResult<T> setTotal (long total){
        this.total = total;
        return this;
    }

    public AjaxPageResult<T> setPageNum (long pageNum){
        this.pageNum = pageNum;
        return this;
    }

    public AjaxPageResult<T> setPageSize (long pageSize){
        this.pageSize = pageSize;
        return this;
    }

}
