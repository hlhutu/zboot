package com.zboot.comm.utils;

import cn.hutool.core.convert.Convert;
import com.zboot.comm.constant.SecurityConstants;

import javax.servlet.http.HttpServletRequest;

/**
 * 权限获取工具类
 *
 * @author zhu
 */
public class SecurityUtils {
    /**
     * 获取用户
     * 注意，如果接口不作token校验，是无法获取用户信息的
     */
    public static String getUsername() {
        String username = ServletUtils.getRequest().getHeader(SecurityConstants.DETAILS_USERNAME);
        if(StringUtils.isNotEmpty(username)) {
            return ServletUtils.urlDecode(username);
        }else{
            return null;
        }
    }

    public static String getRealName() {
        String realName = ServletUtils.getRequest().getHeader(SecurityConstants.DETAILS_REALNAME);
        if(StringUtils.isNotEmpty(realName)) {
            return ServletUtils.urlDecode(realName);
        }else{
            return null;
        }
    }

    public static String getNickName() {
        String nickname = ServletUtils.getRequest().getHeader(SecurityConstants.DETAILS_NICKNAME);
        if(StringUtils.isNotEmpty(nickname)) {
            return ServletUtils.urlDecode(nickname);
        }else{
            return null;
        }
    }

    public static Boolean isVisual() {
        String visualUserId = ServletUtils.getRequest().getHeader(SecurityConstants.DETAILS_VISUAL_USER_ID);
        if(StringUtils.isNotEmpty(visualUserId)) {
            return true;
        }else{
            return false;
        }
    }

    public static Long getVisualUserId() {
        String visualUserId = ServletUtils.getRequest().getHeader(SecurityConstants.DETAILS_VISUAL_USER_ID);
        if(StringUtils.isNotEmpty(visualUserId)) {
            return Convert.toLong(visualUserId);
        }else{
            return null;
        }
    }

    /**
     * 获取用户ID
     */
    public static Long getUserId() {
        return Convert.toLong(ServletUtils.getRequest().getHeader(SecurityConstants.DETAILS_USER_ID));
    }

    /**
     * 获取请求token
     */
    public static String getToken() {
        return getToken(ServletUtils.getRequest());
    }

    /**
     * 根据request获取请求token
     */
    public static String getToken(HttpServletRequest request) {
        String token = ServletUtils.getRequest().getHeader(SecurityConstants.HEADER);
        if(StringUtils.isEmpty(token)){
            token = ServletUtils.getRequest().getParameter(SecurityConstants.HEADER);
        }
        if (StringUtils.isNotEmpty(token) && token.startsWith(SecurityConstants.TOKEN_PREFIX)) {
            token = token.replace(SecurityConstants.TOKEN_PREFIX, "");
        }
        return token;
    }

    /**
     * 是否为管理员
     *
     * @param userId 用户ID
     * @return 结果
     */
    public static boolean isAdmin(Long userId) {
        return userId != null && 1L == userId;
    }

    /**
     * 生成BCryptPasswordEncoder密码
     *
     * @param password 密码
     * @return 加密字符串
     */
//    public static String encryptPassword(String password) {
//        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
//        return passwordEncoder.encode(password);
//    }

    /**
     * 判断密码是否相同
     *
     * @param rawPassword 真实密码
     * @param encodedPassword 加密后字符
     * @return 结果
     */
//    public static boolean matchesPassword(String rawPassword, String encodedPassword) {
//        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
//        return passwordEncoder.matches(rawPassword, encodedPassword);
//    }
}
