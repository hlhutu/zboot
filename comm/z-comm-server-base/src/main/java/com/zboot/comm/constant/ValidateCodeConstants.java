package com.zboot.comm.constant;

public class ValidateCodeConstants {
    /**
     * 验证码 redis key
     */
    public static final String CAPTCHA_CODE_KEY = "CAPTCHA_CODE_KEY:";

    /**
     * 验证码有效期（分钟）
     */
    public static final long CAPTCHA_EXPIRATION = 2;
}
