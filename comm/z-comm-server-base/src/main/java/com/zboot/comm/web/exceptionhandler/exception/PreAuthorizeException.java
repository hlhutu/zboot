package com.zboot.comm.web.exceptionhandler.exception;

/**
 * 权限异常
 *
 * @author zhu
 */
public class PreAuthorizeException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public PreAuthorizeException() {
    }
}
