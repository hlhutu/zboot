package com.zboot.comm.web.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.SneakyThrows;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Entity基类
 *
 * @author zhu
 */
@Data
public class BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 创建者 */
    @TableField(fill = FieldFill.INSERT )
    private String createBy;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT )
    private Date createTime;

    /** 更新者 */
    @TableField(fill = FieldFill.UPDATE )
    private String updateBy;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE )
    private Date updateTime;

    /** 备注 */
    @TableField(exist = false)
    private String remark;

    /** 搜索值 */
    //@TableField(exist = false)
    //private String searchValue;

    /** 请求参数 */
    @TableField(exist = false)
    private Map<String, Object> params = new HashMap<>();

    /**
     * 利用反射赋值
     * @param key
     * @param value
     */
    @SneakyThrows
    public void set(String key, Object value) {
        Class clazz = this.getClass();
        Field field = clazz.getDeclaredField(key);
        field.setAccessible(true);//解除私有限定
        field.set(this, value);
    }

    /**
     * 利用反射获取值
     * @param key
     * @return
     */
    @SneakyThrows
    public Object get(String key) {
        Class clazz = this.getClass();
        Field field = clazz.getDeclaredField(key);
        field.setAccessible(true);//解除私有限定
        return field.get(this);
    }

    /**
     * 转化为子类
     * @param t
     * @param <T>
     * @return
     */
    public <T extends BaseEntity> T convert(T t){
        BeanUtils.copyProperties(this, t);
        return t;
    }
}
