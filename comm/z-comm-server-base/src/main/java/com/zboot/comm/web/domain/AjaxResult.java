package com.zboot.comm.web.domain;

import cn.hutool.http.HttpStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 操作消息提醒
 *
 * @author zhu
 */
@Data
@ApiModel
public class AjaxResult<T> implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 状态码 */
    @ApiModelProperty("状态码，200：成功，50x：服务端错误，40x：客户端错误")
    private int code;

    /** 返回内容 */
    @ApiModelProperty("提示信息，如报错一般有异常信息")
    private String msg;

    /** 数据对象 */
    @ApiModelProperty("数据对象，根据接口不同返回类型不同")
    private T data;

    @ApiModelProperty("时间戳")
    private long timestamp;

    @ApiModelProperty("是否成功")
    private boolean success;

    protected AjaxResult(){
        this.setSuccess(true);
        this.setTimestamp(System.currentTimeMillis());
        this.setCode(cn.hutool.http.HttpStatus.HTTP_OK);
        this.setMsg("操作成功");
    }

    public static AjaxResult success(){
        AjaxResult result = new AjaxResult();
        return result;
    }

    public static <T> AjaxResult<T> success(T data){
        AjaxResult result = new AjaxResult();
        result.setData(data);
        return result;
    }

    public static <T> AjaxResult<T> success(String msg, T data){
        AjaxResult result = new AjaxResult();
        result.setData(data);
        result.setMsg(msg);
        return result;
    }

    public static AjaxResult error(){
        AjaxResult result = new AjaxResult();
        result.setSuccess(false);
        result.setCode(HttpStatus.HTTP_INTERNAL_ERROR);
        result.setMsg("操作失败");
        return result;
    }

    public static AjaxResult error(String msg){
        AjaxResult result = new AjaxResult();
        result.setSuccess(false);
        result.setCode(HttpStatus.HTTP_INTERNAL_ERROR);
        result.setMsg(msg);
        return result;
    }

    public static AjaxResult error(int code, String msg){
        AjaxResult result = new AjaxResult();
        result.setSuccess(false);
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    public AjaxResult setSuccess(boolean success){
        this.success = success;
        return this;
    }

    public AjaxResult setCode(int code){
        this.code = code;
        return this;
    }

    public AjaxResult setMsg(String msg){
        this.msg = msg;
        return this;
    }

    public AjaxResult setTimestamp(long timestamp){
        this.timestamp = timestamp;
        return this;
    }

    public AjaxResult setData(T data){
        this.data = data;
        return this;
    }

}
