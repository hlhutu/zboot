## 通用模块
~~~
封装一些常用工具，此模块下的组件通常需要pom引入后使用
~~~

~~~
devtools 开发者工具
    gen-code 代码生成器，通过本地执行java程序生成代码

z-comm-core 核心工具，包含一些工具类，lombok，mybatis等，大部分服务都需要引入

z-comm-datasource 数据库组件，分布式事务，druid连接池，动态数据源等，默认使用mysql

z-comm-datasource-sharding 数据库组件，基本同上，只不过没有动态数据源，而使用ShardingSphere代替

z-comm-mq 消息队列，使用的RocketMQ
~~~