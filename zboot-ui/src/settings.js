module.exports = {
  title: "畅未数智共享平台",

  /**
   * 侧边栏主题 深色主题theme-dark，浅色主题theme-light
   */
  sideTheme: "theme-dark",

  /**
   * 是否显示右侧的视图设置
   */
  showSettings: false,

  /**
   * 是否使用顶部导航，true=顶部，false=左侧
   */
  topNav: true,

  /**
   * 是否显示 tagsView，显示后才可以多开窗口
   */
  tagsView: true,

  /**
   * 是否固定头部
   */
  fixedHeader: true,

  /**
   * 是否显示logo
   */
  sidebarLogo: true,

  /**
   * @type {string | array} 'production' | ['production', 'development']
   * @description Need show err logs component.
   * The default is only used in the production env
   * If you want to also use it in dev, you can pass ['production', 'development']
   */
  errorLog: "production",

  //baseUrl: "http://192.168.0.21:8080",
  baseUrl: 'http://127.0.0.1:8080',
};
