import Vue from 'vue'
import Router from 'vue-router'
/* Layout */
import Layout from '@/layout'
import newEditor from '@/views/gen/template/newEditor'

Vue.use(Router)

/**
 * Note: 路由配置项
 *
 * hidden: true                   // 当设置 true 的时候该路由不会再侧边栏出现 如401，login等页面，或者如一些编辑页面/edit/1
 * alwaysShow: true               // 当你一个路由下面的 children 声明的路由大于1个时，自动会变成嵌套的模式--如组件页面
 *                                // 只有一个时，会将那个子路由当做根路由显示在侧边栏--如引导页面
 *                                // 若你想不管路由下面的 children 声明的个数都显示你的根路由
 *                                // 你可以设置 alwaysShow: true，这样它就会忽略之前定义的规则，一直显示根路由
 * redirect: noRedirect           // 当设置 noRedirect 的时候该路由在面包屑导航中不可被点击
 * name:'router-name'             // 设定路由的名字，一定要填写不然使用<keep-alive>时会出现各种问题
 * meta : {
    noCache: true                // 如果设置为true，则不会被 <keep-alive> 缓存(默认 false)
    title: 'title'               // 设置该路由在侧边栏和面包屑中展示的名字
    icon: 'svg-name'             // 设置该路由的图标，对应路径src/assets/icons/svg
    breadcrumb: false            // 如果设置为false，则不会在breadcrumb面包屑中显示
  }
 */

// 公共路由
export const constantRoutes = [
  {
    path: '/tinymce',
    name: 'tinymce',
    component: () => import(/* webpackChunkName: "tinymce-example" */'@/components/tinymce/example/Index.vue')
  },
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path(.*)',
        component: (resolve) => require(['@/views/redirect'], resolve)
      }
    ]
  },
  {
    path: '/login',
    component: (resolve) => require(['@/views/login'], resolve),
    hidden: true
  },
  {
    path: '/404',
    component: (resolve) => require(['@/views/error/404'], resolve),
    hidden: true
  },
  {
    path: '/401',
    component: (resolve) => require(['@/views/error/401'], resolve),
    hidden: true
  },
  {
    path: '',
    component: Layout,
    redirect: 'index',
    children: [
      {
        path: 'index',
        component: (resolve) => require(['@/views/index'], resolve),
        name: '首页',
        meta: { title: '首页', icon: 'dashboard', noCache: true, affix: true }
      }
    ]
  },
  {
    path: '/user',
    component: Layout,
    hidden: true,
    redirect: 'noredirect',
    children: [
      {
        path: 'profile',
        component: (resolve) => require(['@/views/system/user/profile/index'], resolve),
        name: 'Profile',
        meta: { title: '个人中心', icon: 'user' }
      }
    ]
  },
  {
    path: '/dict',
    component: Layout,
    hidden: true,
    children: [
      {
        path: 'type/data/:dictId(\\d+)',
        component: (resolve) => require(['@/views/system/dict/data'], resolve),
        name: 'Data',
        meta: { title: '字典数据', icon: '' }
      }
    ]
  },
  {
    path: '/job',
    component: Layout,
    hidden: true,
    children: [
      {
        path: 'log',
        component: (resolve) => require(['@/views/monitor/job/log'], resolve),
        name: 'JobLog',
        meta: { title: '调度日志' }
      }
    ]
  },
  {
    path: '/gen',
    component: Layout,
    hidden: true,
    children: [
      {
        path: 'edit/:tableId(\\d+)',
        component: (resolve) => require(['@/views/tool/gen/editTable'], resolve),
        name: 'GenEdit',
        meta: { title: '修改生成配置' }
      }
    ]
  },
  {
    path: '/newEditor',
    component: newEditor
  },
  {
    path: '/groovy',
    component: Layout,
    hidden: true,
    children: [
      {
        path: 'script/:dataId(\\w+)/:status',
        component: (resolve) => require(['@/views/tool/groovy/script'], resolve),
        name: 'Groovy Script',
        meta: { title: '修改脚本' }
      }
    ]
  },
  {
    path: '/activiti',
    component: Layout,
    hidden: true,
    children: [
      {
        path: 'modeler/:dataId(\\w+)/:status',
        component: (resolve) => require(['@/views/activiti/modeler/modeler'], resolve),
        name: 'Activiti modeler',
        meta: { title: '编辑流程图' }
      }
    ]
  },
  {
    path: '/openform',
    component: Layout,
    hidden: true,
    redirect: 'noredirect',
    children: [
      {
        path: 'design',
        component: (resolve) => require(['@/views/tool/openform/design'], resolve),
        name: 'Form Generator',
        meta: { title: '创建表单' }
      },
      {
        path: 'design/:dataId(\\w+)',
        component: (resolve) => require(['@/views/tool/openform/design'], resolve),
        name: 'Form Generator Editor',
        meta: { title: '编辑表单' }
      },
      {
        path: 'review/:step(\\w+)/:defId(\\w+)',
        component: (resolve) => require(['@/views/tool/openform/review'], resolve),
        name: 'Form Review',
        meta: { title: '表单预览' }
      },
      {
        path: 'reviewByKey/:step(\\w+)/:formKey(\\w+)',
        component: (resolve) => require(['@/views/tool/openform/review'], resolve),
        name: 'Form Review By Key',
        meta: { title: '表单预览' }
      },
      {
        path: 'view/:formId(\\w+)/:dataId(\\w+)',
        component: (resolve) => require(['@/views/tool/openform/reviewFormData'], resolve),
        name: 'FormData View',
        meta: { title: '查看数据' }
      },
      {
        path: 'view/:formId(\\w+)',
        component: (resolve) => require(['@/views/tool/openform/reviewFormData'], resolve),
        name: 'FormData Add',
        meta: { title: '添加数据' }
      },
      {
        path: 'hisList/:formKey(\\w+)',
        component: (resolve) => require(['@/views/tool/openform/hisList'], resolve),
        name: 'Form History',
        meta: { title: '历史版本' }
      },
      {
        path: 'dataHisList/:formKey(\\w+)/:dataId(\\w+)',
        component: (resolve) => require(['@/views/tool/openform/dataHisList'], resolve),
        name: 'Data History',
        meta: { title: '数据历史' }
      },
      {
        path: 'designForPerson/:formDefKey(\\w+)',
        component: (resolve) => require(['@/views/tool/openform/designForPerson'], resolve),
        name: 'Form Generator For Person',
        meta: { title: '表单编辑（人事）' }
      },
    ]
  },
  {
    path: '/person',
    component: Layout,
    hidden: true,
    redirect: 'noredirect',
    children: [
      {
        path: 'userTableForUser/:userId(\\w+)',
        component: (resolve) => require(['@/views/renshi/setting/userTableForUser'], resolve),
        name: 'userTableForUser',
        meta: { title: '用户信息表查看' }
      },
    ]
  },
]

export default new Router({
  mode: 'history', // 去掉url中的#
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})
