import {getAccounts, getInfo, getVisualUsers, login, loginAccount, loginVisual, logout, refreshToken} from '@/api/login'
import {getToken, getTokenChildren, removeToken, removeTokenChildren, setExpiresIn, setToken} from '@/utils/auth'

const user = {
  state: {
    token: getToken(),
    tokenChildren: getTokenChildren(),
    name: '',
    avatar: '',
    roles: [],
    permissions: [],
    userInfo: {}
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_TOKEN_CHILDREN: (state, tokens) => {
      state.tokenChildren = tokens;
    },
    SET_EXPIRES_IN: (state, time) => {
      state.expires_in = time
    },
    SET_NAME: (state, name) => {
      state.name = name
    },
    SET_AVATAR: (state, avatar) => {
      state.avatar = avatar
    },
    SET_ROLES: (state, roles) => {
      state.roles = roles
    },
    SET_PERMISSIONS: (state, permissions) => {
      state.permissions = permissions
    },
    SET_USER_INFO: (state, userInfo) => {
      state.userInfo = userInfo;
    }
  },

  actions: {
    // 登录
    Login({ commit }, userInfo) {
      const username = userInfo.username.trim()
      const password = userInfo.password
      const code = userInfo.code
      const uuid = userInfo.uuid
      return new Promise((resolve, reject) => {
        login(username, password, code, uuid).then(res => {
          let data = res.data
          setToken(data.mainToken.access_token)
          setExpiresIn(data.mainToken.expires_in)

          commit('SET_TOKEN', data.mainToken.access_token)
          commit('SET_EXPIRES_IN', data.mainToken.expires_in)

          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },

    LoginAccount({ commit }, userInfo) {
      return new Promise((resolve, reject) => {
        loginAccount(userInfo.userName).then(res => {
          let data = res.data
          setToken(data.mainToken.access_token)
          setExpiresIn(data.mainToken.expires_in)

          commit('SET_TOKEN', data.mainToken.access_token)
          commit('SET_EXPIRES_IN', data.mainToken.expires_in)

          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },

    LoginVisual({ commit }, userInfo) {
      return new Promise((resolve, reject) => {
        loginVisual(userInfo.userId).then(res => {
          let data = res.data
          setToken(data.mainToken.access_token)
          setExpiresIn(data.mainToken.expires_in)

          commit('SET_TOKEN', data.mainToken.access_token)
          commit('SET_EXPIRES_IN', data.mainToken.expires_in)

          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 获取用户信息
    GetInfo({ commit, state }) {
      return new Promise((resolve, reject) => {
        getInfo().then(res => {
          const user = res.data.user
          const avatar = user.avatar == "" ? require("@/assets/logo.png") : user.avatar;
          if (res.data.roles && res.data.roles.length > 0) { // 验证返回的roles是否是一个非空数组
            commit('SET_ROLES', res.data.roles)
            commit('SET_PERMISSIONS', res.data.permissions)
          } else {
            commit('SET_ROLES', ['ROLE_DEFAULT'])
          }
          commit('SET_NAME', user.userName)
          commit('SET_AVATAR', avatar)
          commit('SET_USER_INFO', user)
          resolve(res)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 查询与我关联的账号
    GetAccounts({commit, state}) {
      return new Promise((resolve, reject) => {
        getAccounts().then(res => {
          resolve(res)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 查询授权给我的虚拟账号
    GetVisualUsers({commit, state}) {
      return new Promise((resolve, reject) => {
        getVisualUsers().then(res => {
          resolve(res)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 刷新token
    RefreshToken({commit, state}) {
      return new Promise((resolve, reject) => {
        refreshToken(state.token).then(res => {
          setExpiresIn(res.data)
          commit('SET_EXPIRES_IN', res.data)
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 退出系统
    LogOut({ commit, state }) {
      return new Promise((resolve, reject) => {
        logout(state.token).then(() => {
          commit('SET_TOKEN', '')
          commit('SET_TOKEN_CHILDREN', []);
          commit('SET_ROLES', [])
          commit('SET_PERMISSIONS', [])
          removeToken()
          removeTokenChildren()
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 前端 登出
    FedLogOut({ commit }) {
      return new Promise(resolve => {
        commit('SET_TOKEN', '')
        removeToken()
        resolve()
      })
    }
  }
}

export default user
