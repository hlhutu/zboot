import request from '@/utils/request'

// 查询草稿列表
export function listDraft(query) {
  return request({
    url: '/openform/list/formDefDraft',
    method: 'get',
    params: query
  })
}
// 查询草稿详情
export function getDraft(id) {
  return request({
    url: '/openform/formDefDraft/'+id,
    method: 'get'
  })
}
// 删除草稿
export function removeDraft(id) {
  return request({
    url: '/openform/formDefDraft/'+id,
    method: 'delete'
  });
}
// 保存草稿
export function saveDraft(query) {
  return request({
    url: '/openform/formDefDraft',
    method: 'post',
    header:{
      'Content-Type':'application/json'  //如果写成contentType会报错
    },
    data: query
  });
}
// 发布草稿
export function publishDraft(id) {
  return request({
    url: '/openform/publish/formDefDraft/'+id,
    method: 'post'
  });
}

// 查询表单json列表
export function listDef(query) {
  return request({
    url: '/openform/list/formDef',
    method: 'get',
    params: query
  })
}

// 查询表单json详情
export function getDef(id) {
  return request({
    url: '/openform/formDef/'+id,
    method: 'get'
  })
}

// 查询表单json详情
export function getDefByKey(key) {
  return request({
    url: '/openform/formDef/key/'+key,
    method: 'get'
  })
}

export function saveDef(key, formDef) {
  return request({
    url: '/openform/save/formDef/key/'+key,
    method: 'post',
    header:{
      'Content-Type':'application/json'  //如果写成contentType会报错
    },
    data: formDef
  })
}

// 查询表单json详情
export function removeDef(defId) {
  return request({
    url: '/openform/formDef/'+defId,
    method: 'delete'
  })
}

// 查询表单json详情
export function getDefByKeyAndVersion(key, version) {
  return request({
    url: '/openform/formDef/key/'+key+"/v/"+version,
    method: 'get'
  })
}

// 查询表单key是否存在
export function exists(key) {
  return request({
    url: '/openform/exists/formDef/'+key,
    method: 'get'
  })
}

// 查询表单json详情
export function getDefHis(id) {
  return request({
    url: '/openform/formDefHis/'+id,
    method: 'get'
  })
}

// 查询表单json列表
export function listDefHis(query) {
  return request({
    url: '/openform/list/formDefHis',
    method: 'get',
    data: query
  })
}

export function loadOptions(requestJson) {
  return request(requestJson)
}
