import request from '@/utils/request'

export function searchData(formKey, keyword) {
  return request({
    url: '/openform/formdata/_search/'+formKey,
    method: 'get',
    params: {
      keyword: keyword
    }
  })
}

export function getDataById(formKey, id) {
  return request({
    url: '/openform/formdata/'+formKey+'/dataId/'+id,
    method: 'get'
  })
}

export function getHistoryById(formKey, dataId) {
  return request({
    url: '/openform/history/formdata/'+formKey+'/dataId/'+dataId,
    method: 'get'
  });
}

export function saveData(formKey, data, dataId) {
  return request({
    url: '/openform/formdata/'+formKey+(dataId?("/dataId/"+dataId):""),
    method: 'post',
    header:{
      'Content-Type':'application/json'  //如果写成contentType会报错
    },
    data: data
  });
}

export function removeData(formKey, dataId) {
  return request({
    url: '/openform/formdata/'+formKey+"/dataId/"+dataId,
    method: 'delete'
  });
}
