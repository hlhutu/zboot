import request from '@/utils/request'

// 登录方法
export function login(username, password, code, uuid) {
  return request({
    url: '/auth/login',
    method: 'post',
    data: { username, password, code, uuid }
  })
}

// 小号登录
export function loginAccount(username) {
  return request({
    url: '/auth/login/account/'+username,
    method: 'post'
  })
}

// 虚拟账号登录
export function loginVisual(visualUserId) {
  return request({
    url: '/auth/login/visual/'+visualUserId,
    method: 'post'
  })
}

// 刷新方法
export function refreshToken() {
  return request({
    url: '/auth/refresh',
    method: 'post'
  })
}

// 获取用户详细信息
export function getInfo() {
  return request({
    url: '/system/user/getInfo',
    method: 'get'
  })
}

export function getAccounts() {
  return request({
    url: '/system/user/getAccounts',
    method: 'get'
  })
}

export function getVisualUsers() {
  return request({
    url: '/system/visual/user/byToUser',
    method: 'get'
  })
}

// 退出方法
export function logout() {
  return request({
    url: '/auth/logout',
    method: 'delete'
  })
}

// 获取验证码
export function getCodeImg() {
  return request({
    url: '/code',
    method: 'get'
  })
}
