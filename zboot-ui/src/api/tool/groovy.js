import request from '@/utils/request'

// 查询列表
export function list(params) {
  return request({
    url: '/groovy/script/list',
    method: 'get',
    params: params
  })
}

// 查询详情
export function get(dataId) {
  return request({
    url: '/groovy/script/' + dataId,
    method: 'get'
  })
}

// 根据key查询groovy详情
export function getByKey(configKey) {
  return request({
    url: '/groovy/script/key/' + configKey,
    method: 'get'
  })
}

// 保存groovy脚本，将会产生新的版本
export function save(data) {
  return request({
    url: '/groovy/script',
    method: 'post',
    data: data
  })
}

// 删除脚本
export function remove(dataId) {
  return request({
    url: '/groovy/script/' + dataId,
    method: 'delete'
  })
}

// 导出列表
export function excel(query) {
  return request({
    url: '/groovy/script/export',
    method: 'get',
    params: query
  })
}

// 导出列表
export function runByKey(key) {
  return request({
    url: '/groovy/run/script/key/' + key,
    method: 'post',
    params: {}
  })
}
