import request from '@/utils/request'

// 创建虚拟账号
export function createVisualUser(data) {
  return request({
    url: '/system/visual/user',
    method: 'post',
    data: data
  })
}

// 修改虚拟账号
export function updateVisualUser(id, data) {
  return request({
    url: '/system/visual/user/'+id,
    method: 'put',
    data: data
  })
}

// 删除虚拟账号
export function removeVisualUser(id) {
  return request({
    url: '/system/visual/user/'+id,
    method: 'delete',
  })
}

// 查询虚拟账号
export function getVisualUser(id) {
  return request({
    url: '/system/visual/user/'+id,
    method: 'get',
  })
}

// 将虚拟账号授权给某人
export function grantVisualUser(id, data) {
  return request({
    url: '/system/grant/visual/user/'+id,
    method: 'post',
    data: data
  })
}

// 查询我的虚拟账号列表
export function listVisualUser() {
  return request({
    url: '/system/visual/user/byFromUser',
    method: 'get'
  })
}

// 查询分配用户列表
export function listGrantList(visualUserId) {
  return request({
    url: '/visual/user/grantList/'+visualUserId,
    method: 'get'
  })
}
