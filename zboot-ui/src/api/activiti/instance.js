import request from '@/utils/request'

// 查询列表
export function listData(params) {
  return request({
    url: '/activiti/activiti/process/list',
    method: 'get',
    params: params
  })
}
