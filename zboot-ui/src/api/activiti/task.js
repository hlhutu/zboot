import request from '@/utils/request'

export function taskDetail(id) {
  return request({
    url: '/activiti/task/'+id,
    method: 'get'
  })
}

/**
 * 待办列表
 * @param query
 */
export function todoList(query) {
  return request({
    url: '/activiti/task/taskTodoList',
    method: 'post',
    header:{
      'Content-Type':'application/json'  //如果写成contentType会报错
    },
    data: query
  })
}

/**
 * 已办列表
 * @param query
 */
export function doneList(query) {
  return request({
    url: '/activiti/task/taskDoneList',
    method: 'get',
    params: query
  })
}

export function claim(id) {
  return request({
    url: '/activiti/task/claim/'+id,
    method: 'post'
  });
}

export function complete(id, taskVo) {
  return request({
    url: '/activiti/task/complete/'+id,
    method: 'post',
    header:{
      'Content-Type':'application/json'  //如果写成contentType会报错
    },
    data: taskVo
  });
}

export function back(id, taskVo) {
  return request({
    url: '/activiti/task/back/'+id,
    method: 'post',
    header:{
      'Content-Type':'application/json'  //如果写成contentType会报错
    },
    data: taskVo
  });
}

export function reject(id, taskVo) {
  return request({
    url: '/activiti/task/reject/'+id,
    method: 'post',
    header:{
      'Content-Type':'application/json'  //如果写成contentType会报错
    },
    data: taskVo
  });
}

export function loadData(busniessKey) {
  return request({
    url: '/activiti/formdata/'+busniessKey,
    method: 'get'
  });
}

export function listHis(procInstId) {
  return request({
    url: '/activiti/activiti/process/listHistory/'+procInstId,
    method: 'get',
    params: {
    }
  });
}

export function getInst(id) {
  return request({
    url: '/activiti/activiti/process/'+id,
    method: 'get'
  })
}
