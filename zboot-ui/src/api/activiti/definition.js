import request from '@/utils/request'

// 查询流程定义列表
export function listDefinition(query) {
  return request({
    url: '/activiti/activiti/definition/list',
    method: 'get',
    params: query
  })
}

// 激活挂起流程定义
export function suspendOrActiveDefinition(data) {
  return request({
    url: '/activiti/activiti/definition/suspendOrActiveDefinition',
    method: 'post',
    params: data
  })
}

// 删除流程定义
export function delDefinition(deploymentId) {
  return request({
    url: '/activiti/activiti/definition/remove/' + deploymentId,
    method: 'delete',
  })
}

// 流程定义转成模型
export function convert2Model(data) {
  return request({
    url: '/activiti/activiti/definition/convert2Model',
    method: 'post',
    params: data
  })
}

export function startDefinition(procDefKey, version, data) {
  return request({
    url: '/activiti/activiti/definition/start/'+procDefKey+'/v/'+version,
    method: 'post',
    header:{
      'Content-Type':'application/json'  //如果写成contentType会报错
    },
    data: data
  });
}

export function listDefinitionsByGroup() {
  return request({
    url: '/activiti/activiti/listByGroup/definition',
    method: 'get'
  });
}

export function getProcDef(id) {
  return request({
    url: '/activiti/activiti/definition/' + id,
    method: 'get'
  });
}
