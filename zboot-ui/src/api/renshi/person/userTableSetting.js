import request from '@/utils/request'

// 查询人员信息表列表
export function getUserTableSettings(params) {
  return request({
    url: '/person/list/userTableSetting',
    method: 'get',
    params: params
  })
}

// 查询人员信息表详情
export function getUserTableSettingByKey(key) {
  return request({
    url: '/person/userTableSetting/key/'+key,
    method: 'get'
  })
}

//保存基本信息
export function saveUserTableSetting(data) {
  return request({
    url: '/person/userTableSetting/base',
    method: 'post',
    header:{
      'Content-Type':'application/json'  //如果写成contentType会报错
    },
    data: data
  })
}

//删除数据
export function deleteUserTableSetting(key) {
  return request({
    url: '/person/userTableSetting/key/'+key,
    method: 'delete'
  })
}

//修改数据
export function updateUserTableSetting(id, updateBean) {
  return request({
    url: '/person/userTableSetting/'+id,
    method: 'put',
    header:{
      'Content-Type':'application/json'
    },
    data: updateBean
  })
}

