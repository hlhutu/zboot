import request from '@/utils/request'

export function getUserTablesForUser(userId) {
  return request({
    url: '/person/userTables/user/'+userId,
    method: 'get'
  })
}

export function getUserTablesForMe(userId) {
  return request({
    url: '/person/userTables/user/',
    method: 'get'
  })
}

