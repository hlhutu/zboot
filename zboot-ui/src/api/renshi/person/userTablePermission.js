import request from '@/utils/request'

// 查询表权限
export function getUserTablePermission(roleKey, formDefKey) {
  return request({
    url: '/person/userTablePermission/'+roleKey+'/'+formDefKey,
    method: 'get'
  })
}

// 查询字段权限
export function getUserColumnPermission(roleKey, formDefKey) {
  return request({
    url: '/person/userTablePermission/columns/'+roleKey+'/'+formDefKey,
    method: 'get'
  })
}

//保存表权限
export function saveUserTablePermission(roleKey, formDefKey, data) {
  return request({
    url: '/person/userTablePermission/'+roleKey+'/'+formDefKey,
    method: 'post',
    header:{
      'Content-Type':'application/json'
    },
    data: data
  })
}

//保存字段权限
export function saveUserColumnPermission(roleKey, formDefKey, data) {
  return request({
    url: '/person/userTablePermission/columns/'+roleKey+'/'+formDefKey,
    method: 'post',
    header:{
      'Content-Type':'application/json'
    },
    data: data
  })
}
