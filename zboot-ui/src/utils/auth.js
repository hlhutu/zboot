import Cookies from 'js-cookie'

const TokenKey = 'Admin-Token'
const TokenChildrenKey = 'Admin-Token-children'

const ExpiresInKey = 'Admin-Expires-In'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function getTokenChildren() {
  let jsonStr = Cookies.get(TokenChildrenKey);
  return jsonStr? JSON.parse(jsonStr): null;
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function setTokenChildren(tokens) {
  return Cookies.set(TokenChildrenKey, tokens)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}

export function removeTokenChildren() {
  return Cookies.remove(TokenChildrenKey)
}

export function getExpiresIn() {
  return Cookies.get(ExpiresInKey) || -1
}

export function setExpiresIn(time) {
  return Cookies.set(ExpiresInKey, time)
}

export function removeExpiresIn() {
  return Cookies.remove(ExpiresInKey)
}
