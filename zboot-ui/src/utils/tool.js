export function closeSelectedTag($this) {
  let view = $this.$router.currentRoute;
  $this.$store.dispatch('tagsView/delView', view).then(({ visitedViews }) => {
    if (isActive($this, view)) {
      toLastView($this, visitedViews, view)
    }
  });
}
function isActive($this, route) {
  return route.path === $this.$route.path
}
function toLastView($this, visitedViews, view) {
  const latestView = visitedViews.slice(-1)[0]
  if (latestView) {
    $this.$router.push(latestView.fullPath)
  } else {
    // now the default is to redirect to the home page if there is no tags-view,
    // you can adjust it according to your needs.
    if (view.name === 'Dashboard') {
      // to reload home page
      $this.$router.replace({ path: '/redirect' + view.fullPath })
    } else {
      $this.$router.push('/')
    }
  }
}
