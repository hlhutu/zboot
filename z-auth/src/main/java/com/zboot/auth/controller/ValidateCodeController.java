package com.zboot.auth.controller;

import com.zboot.comm.web.domain.AjaxResult;
import com.zboot.auth.service.ValidateCodeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@Api(value = "验证码controller", tags = "验证码controller")
@RequestMapping
public class ValidateCodeController {

    @Autowired
    ValidateCodeService validateCodeService;

    @ApiOperation(value = "获取验证码", notes = "支持计算，字符，滑块验证码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "testLog", value = "测试日志表对象", paramType = "json", required = true)
    })
    @GetMapping("/code")
    public AjaxResult code() throws IOException {
        return validateCodeService.createCapcha();
    }
}
