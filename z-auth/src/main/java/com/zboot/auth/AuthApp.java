package com.zboot.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

import java.net.InetAddress;
import java.net.UnknownHostException;

@SpringBootApplication
public class AuthApp {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(AuthApp.class);
        welcome(context.getEnvironment());
    }

    private static void welcome(Environment env) {
        String ip = null;
        try {
            ip = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            ip = "localhost";
        }
        String port = env.getProperty("server.port");
        String path = env.getProperty("server.servlet.context-path") == null? "": env.getProperty("server.servlet.context-path");
        String name = env.getProperty("spring.application.name");
        String profile = env.getProperty("spring.profiles.active");
        System.out.println("\n----------------------------------------------------------\n" +
                "\t启动完成，" + name + ":" + profile + "\n" +
                "\tLocal:\t\thttp://" + ip + ":" + port + path + "/\n" +
                "\t文档:\t\thttp://" + ip + ":" + port + path + "/doc.html\n" +
                "\t数据库监控:\thttp://" + ip + ":" + port + path + "/druid\n" +
                "----------------------------------------------------------");
    }
}
