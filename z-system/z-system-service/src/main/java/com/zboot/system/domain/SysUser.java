package com.zboot.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.zboot.comm.web.domain.BaseEntity;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 用户信息表
 * </p>
 *
 * @author zhu
 * @since 2022-11-21
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "SysUser对象", description = "用户信息表")
public class SysUser extends BaseEntity {


    @ApiModelProperty(value = "用户ID")
    @TableId
    private Long userId;

    @ApiModelProperty(value = "用户账号")
    private String userName;

    @ApiModelProperty(value = "手机号码")
    private String phonenumber;

    @ApiModelProperty(value = "工号")
    private String worknum;

    @ApiModelProperty(value = "证件类型：如身份证")
    private String idnumType;

    @ApiModelProperty(value = "证件号码")
    private String idnum;

    @ApiModelProperty(value = "用户类型：如 普通用户，管理用户")
    private String userType;

    @ApiModelProperty(value = "用户邮箱")
    private String email;

    @ApiModelProperty(value = "用户昵称")
    private String nickName;

    @ApiModelProperty(value = "用户性别（0男 1女 2未知）")
    private String sex;

    @ApiModelProperty(value = "头像地址")
    private String avatar;

    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "帐号状态（0正常 1停用）")
    private String status;

    @ApiModelProperty(value = "删除标志（0代表存在 2代表删除）")
    private String delFlag;

    @ApiModelProperty(value = "最后登录IP")
    private String loginIp;

    @ApiModelProperty(value = "最后登录时间")
    private Date loginDate;

    @ApiModelProperty(value = "创建者")
    private String createBy;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "更新者")
    private String updateBy;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "微信openid")
    private String openid;

    @ApiModelProperty(value = "微信unionid")
    private String unionid;

    @ApiModelProperty(value = "部门ID，科室")
    private Long deptId;

    @ApiModelProperty(value = "定岗科室")
    private Long postDeptId;

    @ApiModelProperty(value = "用户单位")
    private Long belongDeptId;

    @ApiModelProperty(value = "是否小号")
    private Boolean isSmall;

    @ApiModelProperty(value = "小号对应的大号")
    private String mainUserName;

    @ApiModelProperty(value = "账号顺序")
    private Integer accountSort;


}
