package com.zboot.system.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zboot.comm.web.domain.AjaxResult;
import com.zboot.comm.web.page.AjaxPageResult;
import com.zboot.comm.web.page.PageSupport;
import com.zboot.system.domain.SysUser;
import com.zboot.system.service.SysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;


/**
 * 用户信息表 前端控制器
 *
 * @author zhu
 * @since 2022-11-21
 */
@RestController
@Api(value = "用户信息表Controller", tags = "用户信息表Controller")
@RequestMapping
@Slf4j
public class SysUserController {

    @Autowired
    private SysUserService sysUserService;

    /**
     * 添加用户信息表数据
     * @param sysUser
     * @return
     */
    @ApiOperation("保存用户信息表数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sysUser", value = "用户信息表对象", paramType = "json", required = true)
    })
    @PostMapping("/sysUser")
    public AjaxResult save(@RequestBody SysUser sysUser) {
        sysUserService.save(sysUser);
        return AjaxResult.success();
    }

    /**
     * 删除用户信息表数据
     * @param id
     * @return
     */
    @ApiOperation("删除用户信息表数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sysUser", value = "用户信息表对象", paramType = "json", required = true)
    })
    @DeleteMapping("/sysUser/{id}")
    public AjaxResult removeById(@PathVariable("id") Serializable id) {
        sysUserService.removeById(id);
        return AjaxResult.success();
    }

    /**
     * 修改用户信息表数据
     * @param id
     * @param sysUser
     * @return
     */
    @ApiOperation("修改用户信息表数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sysUser", value = "用户信息表对象", paramType = "json", required = true)
    })
    @PostMapping("/sysUser/{id}")
    public AjaxResult updateById(@PathVariable("id") String id, @RequestBody SysUser sysUser) {
        sysUser.setUserId(Long.valueOf(id));
        sysUserService.updateById(sysUser);
        return AjaxResult.success();
    }

    /**
     * 根据id查询用户信息表数据
     * @param id
     * @return
     */
    @ApiOperation("根据id查询用户信息表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sysUser", value = "用户信息表对象", paramType = "json", required = true)
    })
    @GetMapping("/sysUser/{id}")
    public AjaxResult getById(@PathVariable("id") Serializable id) {
        SysUser sysUser = sysUserService.getById(id);
        return AjaxResult.success().setData(sysUser);
    }

    /**
     * 查询用户信息表数据列表，默认分页
     * @param sysUser
     * @return
     */
    @ApiOperation(value = "查询用户信息表数据列表", notes = "默认分页，每页20条")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sysUser", value = "用户信息表对象", paramType = "json", required = true)
    })
    @GetMapping("/sysUser")
    public AjaxResult list(SysUser sysUser) {
        log.info("logstash-查询用户信息表数据列表，来自lombok");
        PageSupport.startPage();
        List<SysUser> list = sysUserService.list(new QueryWrapper<SysUser>().setEntity(sysUser));
        return AjaxPageResult.success(list);
    }
}

