package com.zboot.system;

import com.zboot.comm.autoconfig.EnableMyAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.net.InetAddress;
import java.net.UnknownHostException;

@SpringBootApplication
@EnableMyAutoConfiguration
@EnableScheduling
public class SystemApp {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(SystemApp.class);
        welcome(context.getEnvironment());
    }

    private static void welcome(Environment env) {
        String ip = null;
        try {
            ip = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            ip = "localhost";
        }
        String port = env.getProperty("server.port");
        String path = env.getProperty("server.servlet.context-path") == null? "": env.getProperty("server.servlet.context-path");
        String name = env.getProperty("spring.application.name");
        String profile = env.getProperty("spring.profiles.active");
        System.out.println("\n----------------------------------------------------------\n" +
                "\t启动完成，" + name + ":" + profile + "\n" +
                "\tLocal:\t\thttp://" + ip + ":" + port + path + "/\n" +
                "\t文档:\t\thttp://" + ip + ":" + port + path + "/doc.html\n" +
                "\t数据库监控:\thttp://" + ip + ":" + port + path + "/druid\n" +
                "\t是否能够触发build呢？\n" +
                "----------------------------------------------------------");
    }
}
