package com.zboot.system.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zboot.comm.web.domain.AjaxResult;
import com.zboot.comm.web.page.AjaxPageResult;
import com.zboot.comm.web.page.PageSupport;
import com.zboot.system.domain.TestLog;
import com.zboot.system.service.TestLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;


/**
 * 测试日志表 前端控制器
 *
 * @author zhu
 * @since 2022-08-23
 */
@RestController
@Api(value = "测试日志表controller", tags = "测试日志表controller")
@RequestMapping
public class TestLogController {

    @Autowired
    private TestLogService testLogService;

    /**
     * 添加测试日志表数据
     * @param testLog
     * @return
     */
    @ApiOperation("保存测试日志表数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "testLog", value = "测试日志表对象", paramType = "json", required = true)
    })
    @PostMapping("/testLog")
    public AjaxResult save(@RequestBody TestLog testLog) {
        testLogService.save(testLog);
        return AjaxResult.success();
    }

    /**
     * 删除测试日志表数据
     * @param id
     * @return
     */
    @ApiOperation("删除测试日志表数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "testLog", value = "测试日志表对象", paramType = "json", required = true)
    })
    @DeleteMapping("/testLog/{id}")
    public AjaxResult removeById(@PathVariable("id") Serializable id) {
        testLogService.removeById(id);
        return AjaxResult.success();
    }

    /**
     * 修改测试日志表数据
     * @param id
     * @param testLog
     * @return
     */
    @ApiOperation("修改测试日志表数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "testLog", value = "测试日志表对象", paramType = "json", required = true)
    })
    @PostMapping("/testLog/{id}")
    public AjaxResult updateById(@PathVariable("id") Long id, @RequestBody TestLog testLog) {
        testLog.setId(id);
        testLogService.updateById(testLog);
        return AjaxResult.success();
    }

    /**
     * 根据id查询测试日志表数据
     * @param id
     * @return
     */
    @ApiOperation("根据id查询测试日志表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "testLog", value = "测试日志表对象", paramType = "json", required = true)
    })
    @GetMapping("/testLog/{id}")
    public AjaxResult getById(@PathVariable("id") Serializable id) {
        TestLog testLog = testLogService.getById(id);
        return AjaxResult.success().setData(testLog);
    }

    /**
     * 查询测试日志表数据列表，默认分页
     * @param testLog
     * @return
     */
    @ApiOperation(value = "查询测试日志表数据列表", notes = "默认分页，每页20条")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "testLog", value = "测试日志表对象", paramType = "json", required = true)
    })
    @GetMapping("/testLog")
    public AjaxResult list(TestLog testLog) {
        PageSupport.startPage();
        List<TestLog> list = testLogService.list(new QueryWrapper<TestLog>().setEntity(testLog));
        return AjaxPageResult.success(list);
    }
}

