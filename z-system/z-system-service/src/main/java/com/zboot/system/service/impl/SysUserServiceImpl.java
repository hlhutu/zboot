package com.zboot.system.service.impl;

import com.zboot.system.domain.SysUser;
import com.zboot.system.mapper.SysUserMapper;
import com.zboot.system.service.SysUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户信息表 服务实现类
 * </p>
 *
 * @author zhu
 * @since 2022-11-21
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {

}
