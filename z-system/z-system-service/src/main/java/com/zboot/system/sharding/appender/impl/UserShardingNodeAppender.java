package com.zboot.system.sharding.appender.impl;

import com.zboot.comm.datasource.sharding.appender.ShardingDataNodeAppender;
import lombok.extern.slf4j.Slf4j;
import org.apache.shardingsphere.sharding.rule.TableRule;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class UserShardingNodeAppender implements ShardingDataNodeAppender {
    @Override
    public String getLogicTableName() {
        return null;
    }

    @Override
    public boolean needAppend(TableRule tableRule) {
        return false;
    }

    @Override
    public void append(TableRule tableRule) {

    }
}
