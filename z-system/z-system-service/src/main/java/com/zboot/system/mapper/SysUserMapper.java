package com.zboot.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zboot.system.domain.SysUser;

/**
 * <p>
 * 用户信息表 Mapper 接口
 * </p>
 *
 * @author zhu
 * @since 2022-11-21
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

}
