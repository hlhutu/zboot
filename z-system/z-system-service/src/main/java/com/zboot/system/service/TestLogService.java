package com.zboot.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zboot.system.domain.TestLog;

public interface TestLogService extends IService<TestLog> {
}
