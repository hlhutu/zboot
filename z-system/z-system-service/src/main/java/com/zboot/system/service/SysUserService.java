package com.zboot.system.service;

import com.zboot.system.domain.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户信息表 服务类
 * </p>
 *
 * @author zhu
 * @since 2022-11-21
 */
public interface SysUserService extends IService<SysUser> {

}
