package com.zboot.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zboot.system.domain.TestLog;

public interface TestLogMapper extends BaseMapper<TestLog> {
}
