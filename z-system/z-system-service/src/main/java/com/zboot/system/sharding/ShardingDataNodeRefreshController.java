package com.zboot.system.sharding;

import com.zboot.comm.datasource.sharding.service.ShardingTableRuleService;
import com.zboot.comm.web.domain.AjaxResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;

/**
 * 基于范围分表的 ActualDataNodes 动态刷新JOB
 */
@Slf4j
@Component
@RestController
public class ShardingDataNodeRefreshController {

    @Autowired
    ShardingTableRuleService shardingTableRuleService;

    @PostConstruct//启动立即执行
    //@Scheduled(fixedRate = 1000 * 60 * 1)//1分钟执行一次
    @PostMapping("/actualDataNodes/refresh")
    public AjaxResult refreshActualDataNodes() {
        shardingTableRuleService.refreshActualDataNodes();
        return AjaxResult.success();
    }
}