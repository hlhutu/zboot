package com.zboot.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zboot.system.domain.TestLog;
import com.zboot.system.mapper.TestLogMapper;
import com.zboot.system.service.TestLogService;
import org.springframework.stereotype.Service;

@Service
public class TestLogServiceImpl extends ServiceImpl<TestLogMapper, TestLog> implements TestLogService {
}
