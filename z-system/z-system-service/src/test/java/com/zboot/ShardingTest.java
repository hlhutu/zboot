package com.zboot;

import com.zboot.system.SystemApp;
import org.apache.shardingsphere.driver.jdbc.core.datasource.ShardingSphereDataSource;
import org.apache.shardingsphere.infra.metadata.ShardingSphereMetaData;
import org.apache.shardingsphere.mode.metadata.MetaDataContexts;
import org.apache.shardingsphere.sharding.rule.ShardingRule;
import org.apache.shardingsphere.sharding.rule.TableRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.lang.reflect.Field;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SystemApp.class)
public class ShardingTest {

    @Autowired
    private ShardingSphereDataSource shardingSphereDataSource;

    //所有的规则
    private ShardingRule shardingRule;

    @Test
    public void run() {
        MetaDataContexts metaDataContexts = shardingSphereDataSource.getContextManager().getMetaDataContexts();
        ShardingSphereMetaData logicDb = metaDataContexts.getMetaData("logic_db");

        shardingRule = logicDb.getRuleMetaData().findSingleRule(ShardingRule.class).get();

        Map<String, TableRule> tableRules = shardingRule.getTableRules();
        tableRules.forEach((k, config) -> {
            System.out.println("=== " + k);
            config.getActualDataNodes().forEach(System.out::println);
        });
    }

}
