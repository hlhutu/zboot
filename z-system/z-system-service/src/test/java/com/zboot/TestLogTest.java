package com.zboot;

import cn.hutool.core.date.DateUtil;
import com.zboot.system.SystemApp;
import com.zboot.system.domain.TestLog;
import com.zboot.system.service.TestLogService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SystemApp.class)
public class TestLogTest {

    @Autowired
    private TestLogService testLogService;

    @Test
    public void testSave() {
        for (int i = 1; i <= 12; i++) {
            Date date = new Date();
            TestLog testLog = new TestLog();
            testLog.setName(DateUtil.format(date, "MM-dd HH:mm:ss:SSS"));
            testLog.setWaitTime(date.getTime());
            testLog.setMonth("2023-01");
            testLogService.save(testLog);
        }
    }

    @Test
    public void testQuery() {
        List<TestLog> list = testLogService.list();
        System.out.println("查询到"+list.size()+"条数据");
        list.forEach(System.out::println);
    }
}
