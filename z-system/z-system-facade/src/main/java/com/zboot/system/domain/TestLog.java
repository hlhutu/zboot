package com.zboot.system.domain;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(value = "测试日志", description = "")
public class TestLog {

    private Long id;

    private String name;

    private Long waitTime;

    private String month;
}
